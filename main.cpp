#include <GL/freeglut_std.h>
#include <iostream>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>

using namespace std;

// global variables
int width = 400;
int height = 400;
float px = 0.0f;
float py = 0.0f;
float pr = 1.0f;
float pg = 0.0f;
float pb = 0.0f;
float pSize = 20.0f;

// convert window coordinates to Cartesian coordinates
void windowToScene(float& x, float& y) {
    x = (2.0f * (x / float(width))) - 1.0f;
    y = 1.0f - (2.0f * (y / float(height)));
}

void drawPoint(float x, float y, float size, float r, float g, float b) {
    glColor3f(r, g, b);
    glPointSize(size);

    // Objects to be drawn go here
    glBegin(GL_POINTS);
        glVertex2f(x, y);
    glEnd();
}

// draw the scene
void drawScene() {
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POINT_SMOOTH);
    drawPoint(px, py, pSize, pr, pg, pb);
    drawPoint(px, py, pSize/3, 1.0f, 1.0f, 1.0f);

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
    // button: 0 -> left mouse button, 2 -> right mouse button
    // state: 0 -> click down, 1 -> releasing

    // convert mouse click coordinates from window relative to cartesian
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (state == 0) {
        pSize = 10.0f;
    }
    if (state == 1) {
        pSize = 20.0f;
    }

    // update global point position
    px = mx;
    py = my;

    // draw the scene
    glutPostRedisplay();
}

void motion(int x, int y) {
    // convert mouse drag coordinates from window relative to cartesian
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    px = mx;
    py = my;

    // draw the scene
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
    if (key == 'r') {
        pr = 1.0f;
        pg = 0.0f;
        pb = 0.0f;
    }
    if (key == 'g') {
        pr = 0.0f;
        pg = 1.0f;
        pb = 0.0f;
    }
    if (key == 'b') {
        pr = 0.0f;
        pg = 0.0f;
        pb = 1.0f;
    }

    glutPostRedisplay();
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(width,height);
    glutInitWindowPosition(200, 100);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    glutMouseFunc(mouse);

    glutMotionFunc(motion);

    glutKeyboardFunc(keyboard);
    
    // Run the program
    glutMainLoop();

    return 0;
}